<?php
/**
 * Template for the blog landing page.
 */
$sticky = get_option( 'sticky_posts' )[0];

get_header();
?>
  <div class="container">
    <div class="row">
      <div class="col xs12 l3 push-l9">
        <?php if ( have_rows('highlighted_posts', get_option( 'page_for_posts', true )) ): ?>
          <section
            class="highlighted"
            data-section-type="highlighted"
          >
            <h3 class="highlighted__title"><?php _e('Highlighted', 'iiko'); ?></h3>

            <svg viewBox="0 0 20 40" class="icon icon__highlighted" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <defs>
                <path id="a" d="M1235 41h20v40h-20z"/>
              </defs>
              <g transform="translate(-1235 -41)" fill="none" fill-rule="evenodd">
                <image x="1235" y="41" width="20" height="40" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAoCAYAAAD+MdrbAAAAAXNSR0IArs4c6QAAAKZJREFUSA3t11EKgCAMBmCNLtStwgvVrTqSuXRgM11NoZcNJAX9tJ8ezB7GeDOwpoHWRSnYn6hmqBkKEtDPRhAaWaIZkkAEQ81QEBpZohmSQATD3zLcw2GhsTWzMyLksnlr1i+6HAincku6Q4a7JMJVtJXhDYOjJBjQ6uvXwAIDEIpDn8AqFsk2SkEW49AcfI01UfgLCG0LzeLEr09YmwxvejHcHNET/9cq8/erzfsAAAAASUVORK5CYII="/>
                <use fill-opacity="0" fill="#000" xlink:href="#a"/>
              </g>
            </svg>

            <div class="highlighted__items" js-highlighted="items">
              <?php
              while ( have_rows('highlighted_posts', get_option( 'page_for_posts', true )) ): the_row();

                $post = get_sub_field('post');
                setup_postdata($post);
              ?>
                <div class="card card--snippet" js-highlighted="item">
                  <?php if ( has_post_thumbnail() && get_row_index() === 1 ): ?>
                    <div
                      class="card__thumbnail lazyload"
                      data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
                    >
                    </div>
                  <?php endif; ?>

                  <a class="card__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                  <p class="card__excerpt"><?php echo substr( get_the_excerpt(), 0, 94 ); ?>...</p>

                  <div class="card__meta">
                    <time datetime="<?php the_date( DATE_W3C ); ?>">
                      <?php echo get_the_date('l, jS F Y'); ?>
                    </time>
                  </div>
                </div>
              <?php
                wp_reset_postdata();
              endwhile;
              ?>
            </div>
          </section>
        <?php endif; ?>
      </div>

      <div class="col xs12 l9 pull-l3">
        <h1 class="blog__title"><?php echo get_the_title( get_option( 'page_for_posts', true ) ); ?></h1>

        <?php
        if ( isset( $sticky ) ):
          setup_postdata( $sticky );
        ?>
          <div class="card card--featured">
            <div class="card__body">
              <a class="card__link" href="<?php the_permalink(); ?>"></a>
              <a class="card__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
              <p class="card__excerpt"><?php echo substr( get_the_excerpt(), 0, 200 ); ?>...</p>

              <div class="card__meta">
                <time datetime="<?php the_date( DATE_W3C ); ?>">
                  <?php echo get_the_date('l, jS F Y'); ?>
                </time>
              </div>
            </div>

            <div
              class="card__thumbnail lazyload"
              data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
            >
              <a class="card__link" href="<?php the_permalink(); ?>"></a>
            </div>
          </div>
        <?php
          wp_reset_postdata();
        endif;

        $post = get_option( 'page_for_posts', true );

        setup_postdata( $post );
          get_template_part('template-parts/section', 'downloads');
        wp_reset_postdata();

        if ( have_posts() ): ?>
          <div class="blog__grid row">
            <?php
            /**
             * Start the loop.
             */
            while ( have_posts() ): the_post();
            ?>
              <div class="col xs12 m6 l4">
                <?php get_template_part('template-parts/archive', 'post'); ?>
              </div>
            <?php endwhile; ?>
          </div>

          <nav class="blog__pagination pagination" role="navigation">
            <?php
            /**
             * Display the pagination.
             */
            echo paginate_links(array(
              'next_text' => __('Next', 'iiko'),
              'prev_text' => __('Previous', 'iiko')
            ));
            ?>
          </nav>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php get_footer(); ?>
