<section class="tall-hero tall-hero-desktop" <?php if ( $module['background_image']){ ?>
style="background-image: url(<?php echo $module['background_image']; ?>)" <?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col xs12 m6">
        <div class="tall-hero__content">
          <h2 class="tall-hero__title"> <?php if ( $module['title']){ ?><?php echo $module['title']; } ?></h2>
          <p class="tall-hero__text"> <?php if ( $module['content']){ ?><?php echo $module['content']; } ?></p>
          <?php
            if($module['button_link']){
            $link = $module['button_link'];
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>
        </div>
      </div>
    </div>
  </div> 
</section>
<section class="tall-hero tall-hero-mobile" <?php if ( $module['background_image_mobile']){ ?>
style="background-image: url(<?php echo $module['background_image_mobile']; ?>)" <?php } ?>>
  <div class="container">
    <div class="row">
      <div class="col xs12 m6">
        <div class="tall-hero__content">
         <h2 class="tall-hero__title"> <?php if ( $module['title']){ ?><?php echo $module['title']; } ?></h2>
          <p class="tall-hero__text"> <?php if ( $module['content']){ ?><?php echo $module['content']; } ?></p>
          <?php
            if($module['button_link']){
            $link = $module['button_link'];
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>
        </div>
      </div>
    </div>
  </div> 
</section>