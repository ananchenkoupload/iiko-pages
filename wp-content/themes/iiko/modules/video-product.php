<section
  class="video-carousel video-product"
  data-section-type="video-carousel"
>
  <div class="container">
    <div class="row video-product-row">
        <div class="col xs7 mobile-row-xs7" <?php if($module['video_position'] == 'right'){ ?>style="order:2;" <?php } ?>>
        <?php if ( ! empty( $module['video_carousel_cards'] ) ): ?>
          <div class="video-carousel__carousel carousel" js-video-carousel="carousel">
            <?php foreach ( $module['video_carousel_cards'] as $video_cards ) { ?>
              <div class="video-carousel__card" js-video-carousel="cell">
                <?php if ( $video_cards['video_id'] ): ?>
                  <div
                    class="video-carousel__card-player"
                    data-video="<?php echo $video_cards['video_id']; ?>"
                    js-video-carousel="player"
                  >
                  </div>
                <?php endif; ?>

                <div class="video-carousel__card-body">
                  <?php if ( $video_cards['title'] ): ?>
                    <h4 class="video-carousel__card-title"><?php echo $video_cards['title']; ?></h4>
                  <?php endif; ?>
                </div>
              </div>
            <?php } ?>
          </div>
        <?php endif; ?>
      </div>
        <div class="col xs5 mobile-row-xs5">
          <div class="left-section">
            <?php if ( $module['video_carousel_title'] ): ?>
              <h3 class="video-carousel__title title-line-pattern"><?php echo $module['video_carousel_title']; ?></h3>
            <?php endif; ?>

            <?php if ( $module['video_carousel_content'] ): ?>
              <p class="video-carousel__content"><?php echo $module['video_carousel_content']; ?></p>
            <?php endif; ?>
           </div>
         </div>
    </div>
  </div>
</section>