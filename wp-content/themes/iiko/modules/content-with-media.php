        <section class="content-with-media content-with-media--<?php echo $module['media_position']; ?>">
        <div class="container">
            <div class="row align-item-center">
            <div class="col xs12 m6 content-with-media-content">
                <div class="content-with-media__content">
                <?php if($module['content_with_media_title']){ ?>
                <h2 class="content-with-media__title title-line-pattern"><?php echo $module["content_with_media_title"]; ?></h2>
                <?php } 
                         
                 if($module['content_with_media_description']){ ?>
                <p class="content-with-media__text"><?php echo $module['content_with_media_description']; ?></p>
                <?php } ?>
                </div>
            </div>
            <div class="col xs12 m6 content-with-media-media">
                <div class="content-with-media__media">                
                    <?php if($module['content_with_media_image']){ ?>
                        <img src="<?php echo $module['content_with_media_image']; ?>">
                    <?php } ?>
                </div>
            </div>
            </div>
        </div>
        </section>
