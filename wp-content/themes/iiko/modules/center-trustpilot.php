<section class="center-trustpilot">
  <div class="container">
    <div class="center-trustpilot__header">
      <?php if($module['tpc_header_image']){ ?>
      <img class="center-trustpilot__header-image" alt="Trusted. Proven. Experienced." src="<?php echo $module['tpc_header_image']; ?>">
      <?php } ?>
      <?php if($module['tpc_title']){ ?>
      <h3 class="center-trustpilot__title"><?php echo $module['tpc_title']; ?></h3>
      <?php } ?>
    </div>  
    <div class="row">
    <?php if ( ! empty( $module['trust_pilot_cards'] ) ){ ?>
        <?php foreach ( $module['trust_pilot_cards'] as $cards ) { ?>
            <div class="col xs12 m6">
                <div class="center-trustpilot-column">
                <h5 class="center-trustpilot-column__title"><?php echo $cards['tpc_card_title']; ?></h5>
                <h6 class="center-trustpilot-column__sub-title"><?php echo $cards['tpc_card_sub_title']; ?></h6>
                <p><?php echo $cards['tpc_card_content']; ?></p>
                <img src="<?php echo $cards['tpc_card_image']; ?>">
                </div>
            </div>
    <?php } 
     } ?>
    </div>
  </div>  
</section>