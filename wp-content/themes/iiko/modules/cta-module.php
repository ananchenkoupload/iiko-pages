<?php if($module['show_hide'] == 'yes'){ ?>
<section class="cta-module">
  <div class="container">
    <div class="cta-module__info">
    <?php if($module['cta_title']){ ?>
      <h2 class="cta-module__title"><?php echo $module['cta_title']; ?></h2>
    <?php } ?>
    <?php if($module['cta_description']){ ?>
      <p><?php echo $module['cta_description']; ?></p>
    <?php } ?>

    <?php
            if($module['cta_button']){
            $link = $module['cta_button'];
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>
    </div>
  </div>
</section>
<?php } ?>