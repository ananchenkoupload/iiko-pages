<?php if($module['show_hide'] == 'yes'){ ?>
<section class="media-long-content">
  <div class="container">
    <div class="row">
      <div class="col xs12 l4 <?php if($module['image_position'] == 'right'){ ?> media-long-content__right <?php } ?>">
        <div class="media-long-content__media">
          <?php if($module['media_image']){ ?>
            <img src="<?php echo $module['media_image']; ?>" class="media-long-content__desk-img">
          <?php } ?>
          <?php if($module['media_image_mobile']){ ?>
             <img src="<?php echo $module['media_image_mobile']; ?>" class="media-long-content__mobile-img">
          <?php } ?>
        </div>
      </div>
      <div class="col xs12 l8">
        <div class="media-long-content__content">
        <?php if($module['media_title']){ ?>
          <h2 class="media-long-content__title title-line-pattern"><?php echo $module['media_title']; ?></h2>
        <?php } ?>
        <?php if($module['media_description']){ ?>
          <p class="media-long-content__text"><?php echo $module['media_description']; ?></p>
        <?php } ?>
        <div class="media-long-content__info">
        <div class="media-long-content__column">
         <?php if ( ! empty( $module['media_long_content'] ) ) {
            $i=1;
            foreach ( $module['media_long_content'] as $long_content ) {
           ?> 
                <?php if($long_content['media_body_title']){ ?>
                <strong><?php echo $long_content['media_body_title']; ?></strong>
                <?php } ?>
                <?php if($long_content['media_body_description']){ ?>
                <p><?php echo $long_content['media_body_description']; ?></p>
                <?php } ?>
                <?php if($i%3 == 0){ ?>
                  </div>
                  <div class="media-long-content__column">
                <?php } ?>
                <?php $i++; ?>
          <?php } ?>
        <?php } ?>
        </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>