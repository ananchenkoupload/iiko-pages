<section
  class="latest-news latest-news-blog"
  data-section-type="latest-news"
>
  <div class="latest-news__container container">
    <div class="row">
      <div class="col xs12 l5">
        <div class="content-with-media__content latest-news-blog__content">
        <?php if ( $module['latest_news_title'] ): ?>
        <h2 class="content-with-media__title title-line-pattern"><?php echo $module['latest_news_title']; ?></h2>
        <?php endif; ?>
        <?php if ( $module['latest_news_content'] ): ?>
           <?php echo $module['latest_news_content']; ?>
        <?php endif; ?>
       </div>
      </div>

      <div class="col xs12 l7">
       <?php if ( ! empty( $module['latest_news_highlighted'] ) ) { ?>

          <div class="latest-news__highlighted">
            <div class="latest-news__carousel carousel" js-latest-news="carousel" >
              <?php
              //print '<pre>';
              //print_r($module['latest_news_highlighted']);
              foreach ($module['latest_news_highlighted'] as $latest_news) {
                //print_r($latest_news['post'] );
                //setup_postdata( $latest_news['post'] );
                $latest_news = $latest_news['post'];
              ?>
                <div class="latest-news__card" js-latest-news="cell">
                  <?php if ( get_the_post_thumbnail_url($latest_news->ID) ): ?>
                    <a
                      class="latest-news__card-thumbnail lazyload"
                      data-bgset="<?php echo get_the_post_thumbnail_url($latest_news->ID); ?>"
                      href="<?php echo esc_attr( get_permalink( $latest_news->ID ) ); ?>"
                    >
                    </a>
                  <?php endif; ?>

                  <div class="latest-news__card-body">
                    <a class="latest-news__card-link" href="<?php echo esc_attr( get_permalink( $latest_news->ID ) ); ?>">
                      <h4 class="latest-news__card-title"><?php echo $latest_news->post_title ?></h4>
                    </a>

                    <?php if (  $latest_news->post_content ): ?>
                      <p class="latest-news__card-content"><?php echo strip_tags(substr($latest_news->post_content, 0, 200)); ?>...</p>
                    <?php endif; ?>

                    <p class="latest-news__card-date"><?php echo date('l, jS F Y',strtotime($latest_news->post_date)); ?></p>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>
