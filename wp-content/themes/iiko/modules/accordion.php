

<section class="content-with-accordion">
  <div class="container">
    <div class="row">
      <div class="col xs12 m6 content-with-accordion-media">
        <div class="content-with-accordion__media">
        <?php if($module['accordian_image']){ ?>
          <img src="<?php echo $module['accordian_image']; ?>">
        <?php } ?>
        </div>
      </div>
      <div class="col xs12 m6 content-with-accordion-content">
        <div class="content-with-accordion__content">
        <?php if($module['accordian_title']){ ?>
          <h2 class="content-with-accordion__title title-line-pattern"><?php echo $module['accordian_title']; ?></h2>
        <?php } ?>
        <div class="accordionWrapper">
          <?php if ( ! empty( $module['accordian_tabs'] ) ) {
            foreach ( $module['accordian_tabs'] as $tabs ) {
           ?>
              <div class="accordionItem close">
                <h2 class="accordionItemHeading"><span><?php echo $tabs['accordion_sub_title']; ?><span></h2>
                <div class="accordionItemContent">
                  <?php echo $tabs['accordion_description']; ?>
                </div>
              </div>
            <?php }  ?>
          <?php }  ?>
         </div>
        </div>
      </div>
    </div>
  </div>
</section>