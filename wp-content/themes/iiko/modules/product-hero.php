<section class="product-hero">
  <div class="product-hero__pattern"></div>
  <div class="container">
    <div class="row">
      <div class="col xs12 l6">
        <div class="product-hero__image">
        <?php if($module['product_hero_image']){ ?>
          <img src="<?php echo $module['product_hero_image']; ?>">
        <?php } ?>
        </div>
      </div>
      <div class="col xs12 l6">
        <div class="product-hero__content">
        <?php if($module['product_hero_title']){ ?>
          <h2 class="product-hero__title"><?php echo $module['product_hero_title']; ?></h2>
        <?php } ?>
        <?php if($module['product_hero_subtitle']){ ?>
          <h3 class="product-hero__sub-title"><?php echo $module['product_hero_subtitle']; ?></h3>
        <?php } ?>
        <?php if($module['product_hero_text']){ ?>
          <p class="product-hero__text"><?php echo $module['product_hero_text']; ?></p>
        <?php } ?>
          <?php
             if($module['product_hero_list']){
               echo $module['product_hero_list'];
             }
           ?>
          <div class="product-hero__pricing-cart">
            
          <?php
            if($module['product_hero_link']){
            $link = $module['product_hero_link'];
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>

            <div class="product-hero__pricing-box">
            <?php if($module['product_hero_price']){ ?>
              <h4 class="product-hero__price"><?php echo $module['product_hero_price']; ?></h4>
            <?php } ?>
            <?php if($module['product_hero_currency']){ ?>
              <span><?php echo $module['product_hero_currency']; ?>
            <?php } ?>

              <br/>
              <?php if($module['product_hero_value']){ ?>
              <?php echo $module['product_hero_value']; ?></span>
              <?php } ?>
            </div>
          </div>
          <?php if($module['product_hero_status']){ ?>
          <h5 class="product-hero__status"><?php echo $module['product_hero_status']; ?></h5>
          <?php } ?>
        </div>
      </div>
    </div>
  </div> 
</section>