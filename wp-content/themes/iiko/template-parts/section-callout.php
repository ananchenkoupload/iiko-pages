<section
  class="callout"
  data-section-type="callout"
>
  <div class="callout__container">
    <?php if ( get_field('callout_title') ): ?>
      <div class="container">
        <div class="row">
          <div class="col xs12">
            <h3 class="callout__title"><?php the_field('callout_title'); ?></h3>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div class="callout__row">
      <div
        class="callout__image lazyload"
        data-bgset="<?php the_field('callout_image'); ?>"
      >
      </div>

      <div class="callout__content">
        <div class="callout__content-inner"><?php the_field('callout_content'); ?></div>
      </div>
    </div>

    <div class="callout__form-container">
      <?php if ( get_field('callout_form_title') ): ?>
        <h4 class="callout__form-title"><?php the_field('callout_form_title'); ?></h4>
      <?php endif; ?>

      <div class="callout__form">
        <!--[if lte IE 8]>
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
        <![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
        hbspt.forms.create({
        portalId: "6316159",
        formId: "1806fcff-83a3-4b75-9e82-ecb9cbb8ee7b"
        });
        </script>
      </div>

      <!-- <form class="callout__form">
        <div class="form__field form__field--border">
          <input
            id="Name"
            aria-label="<?php _e('Full Name'); ?>"
            name="NAME"
            placeholder="<?php _e('Full Name'); ?>"
            type="text"
          >
        </div>

        <div class="form__field form__field--border">
          <input
            id="Email"
            aria-label="<?php _e('Email'); ?>"
            name="EMAIL"
            placeholder="<?php _e('Email'); ?>"
            type="email"
          >
        </div>

        <div class="form__field form__field--border">
          <input
            id="Phone"
            aria-label="<?php _e('Phone Number'); ?>"
            name="PHONE"
            placeholder="<?php _e('Phone Number'); ?>"
            type="number"
          >
        </div>

        <div class="form__field">
          <button class="callout__submit button" type="submit">
            <?php the_field('callout_form_submit'); ?>
          </button>
        </div>
      </form> -->
    </div>
  </div>
</section>
