

<section class="content-with-accordion">
  <div class="container">
    <div class="row">
      <div class="col xs12 m6 content-with-accordion-media">
        <div class="content-with-accordion__media">
        <?php if(get_field('accordian_image')){ ?>
          <img src="<?php echo get_field('accordian_image'); ?>">
        <?php } ?>
        </div>
      </div>
      <div class="col xs12 m6 content-with-accordion-content">
        <div class="content-with-accordion__content">
        <?php if(get_field('accordian_title')){ ?>
          <h2 class="content-with-accordion__title title-line-pattern"><?php echo get_field('accordian_title'); ?></h2>
        <?php } ?>
        <div class="accordionWrapper">
          <?php if( have_rows('accordian_tabs') ){ ?>
            <?php while ( have_rows('accordian_tabs') ) : the_row(); ?>
              <div class="accordionItem close">

                <h2 class="accordionItemHeading"><span><?php echo the_sub_field('accordion_sub_title'); ?></span></h2>
                <div class="accordionItemContent">
                <?php echo the_sub_field('accordion_description'); ?>       
                </div>
              </div>
        <?php endwhile; }  ?>
         </div>
        </div>
      </div>
    </div>
  </div>
</section>