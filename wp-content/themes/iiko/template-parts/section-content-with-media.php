 <?php if(get_field('show_hide') == 'yes'){ ?>
    <?php if(have_rows('content_with_media')){ 
        while ( have_rows('content_with_media') ): the_row(); ?>
        <section class="content-with-media content-with-media--<?php echo the_sub_field('media_position'); ?>" data-section-type="latest-news">
        <div class="container">
            <div class="row align-item-center">
            <div class="col xs12 m6 content-with-media-content">
                <div class="content-with-media__content">
                <?php if(get_sub_field('content_with_media_title')){ ?>
                <h2 class="content-with-media__title title-line-pattern"><?php echo the_sub_field("content_with_media_title"); ?></h2>
                <?php } 
                         
                 if(get_sub_field('content_with_media_description')){ ?>
                <p class="content-with-media__text"><?php echo the_sub_field('content_with_media_description'); ?></p>
                <?php } ?>
                </div>
            </div>
            <div class="col xs12 m6 content-with-media-media">
                <div class="content-with-media__media">                
                    <?php if(get_sub_field('content_with_media_image')){ ?>
                        <img src="<?php echo the_sub_field('content_with_media_image'); ?>">
                    <?php } ?>
                </div> 
              </div>
            </div>
        </div>
        </section>
    <?php endwhile; ?>
    <?php } } ?>