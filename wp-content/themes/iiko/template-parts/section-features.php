<section
  class="features"
  data-section-type="features"
>
  <div class="container features__container">
    <div class="row">
      <div class="col xs12 l6">
        <?php if ( get_field('features_header_image') ): ?>
          <img class="features__header" src="<?php the_field('features_header_image'); ?>">
        <?php endif; ?>

        <?php if ( get_field('features_title') ): ?>
          <h3 class="features__title"><?php the_field('features_title'); ?></h3>
        <?php endif; ?>

        <?php if ( get_field('features_content') ): ?>
          <p class="features__content"><?php the_field('features_content'); ?></p>
        <?php endif; ?>

        <?php if ( have_rows('features_feature_cards') ): ?>
          <div class="features__carousel carousel" js-features="carousel">
            <?php while ( have_rows('features_feature_cards') ): the_row(); ?>
              <div class="features__card" js-features="cell">
                <?php if ( get_sub_field('image') ): ?>
                  <?php if ( get_sub_field('link') ): ?>
                    <a href="<?php the_sub_field('link'); ?>">
                  <?php endif; ?>
                      <div
                        class="features__card-thumbnail lazyload"
                        data-bgset="<?php the_sub_field('image'); ?>"
                      >
                      </div>
                  <?php if ( get_sub_field('link') ): ?>
                    </a>
                  <?php endif; ?>
                <?php endif; ?>

                <div class="features__card-body">
                  <?php if ( get_sub_field('title') ): ?>
                    <h4 class="features__card-title"><?php the_sub_field('title'); ?></h4>
                  <?php endif; ?>

                  <?php if ( get_sub_field('content') ): ?>
                    <p class="features__card-content"><?php the_sub_field('content'); ?></p>
                  <?php endif; ?>

                  <?php if ( get_sub_field('link') ): ?>
                    <a class="features__card-link" href="<?php the_sub_field('link'); ?>">
                      <?php _e('Read More', 'iiko'); ?>
                    </a>
                  <?php endif; ?>
                </div>
              </div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>

        <?php if ( get_field('features_button_label') ): ?>
          <a class="button" href="<?php the_field('features_button_link'); ?>">
            <?php the_field('features_button_label'); ?>
          </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>