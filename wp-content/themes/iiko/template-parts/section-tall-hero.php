<section class="tall-hero tall-hero-desktop" <?php if ( get_field('background_image')){ ?>
style="background-image: url(<?php echo get_field('background_image'); ?>)" <?php } ?>>

  <div class="container">
    <div class="row">
      <div class="col xs12 m6">
        <div class="tall-hero__content">
          <h2 class="tall-hero__title"> <?php if ( get_field('title')){ ?><?php echo get_field('title'); } ?></h2>
          <p class="tall-hero__text"> <?php if ( get_field('content')){ ?><?php echo get_field('content'); } ?></p>
          <?php
            if(get_field('button_link')){
            $link = get_field('button_link');
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>
        </div>
      </div>
    </div>
  </div> 
</section>

<section class="tall-hero tall-hero-mobile" <?php if ( get_field('background_image_mobile')){ ?>
style="background-image: url(<?php echo get_field('background_image_mobile'); ?>)" <?php } ?>>

  <div class="container">
    <div class="row">
      <div class="col xs12 m6">
        <div class="tall-hero__content">
          <h2 class="tall-hero__title"> <?php if ( get_field('title')){ ?><?php echo get_field('title'); } ?></h2>
          <p class="tall-hero__text"> <?php if ( get_field('content')){ ?><?php echo get_field('content'); } ?></p>
          <?php
            if(get_field('button_link')){
            $link = get_field('button_link');
            ?>
            <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
            <?php } ?>
        </div>
      </div>
    </div>
  </div> 
</section>