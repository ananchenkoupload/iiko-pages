<section
  class="hero lazyload"
  <?php if ( get_field('hero_image') ): ?>
    data-bgset="<?php the_field('hero_image'); ?>"
  <?php endif; ?>
  data-section-type="hero"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <div class="hero__content">
          <?php if ( !empty( get_field('hero_header_image') ) ): ?>
            <img
              class="hero__header-image lazyload"
              alt="<?php echo get_field('hero_header_image')['alt']; ?>"
              data-src="<?php echo get_field('hero_header_image')['url']; ?>"
            >
          <?php endif; ?>

          <?php if ( get_field('hero_title') ): ?>
            <h1 class="hero__title">
              <?php the_field('hero_title'); ?><br />
              <?php the_field('hero_title_2'); ?>
            </h1>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>