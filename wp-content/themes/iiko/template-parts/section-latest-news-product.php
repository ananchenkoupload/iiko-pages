<section
  class="latest-news latest-news-blog"
  data-section-type="latest-news"
>
  <div class="latest-news__container container">
    <div class="row">
      <div class="col xs12 l5">
        <div class="content-with-media__content latest-news-blog__content">
        <?php if ( get_field('latest_news_title') ): ?>
        <h2 class="content-with-media__title title-line-pattern"><?php the_field('latest_news_title'); ?></h2>
        <?php endif; ?>
        <?php if ( get_field('latest_news_content') ): ?>
           <?php echo get_field('latest_news_content'); ?>
        <?php endif; ?>
       </div>
      </div>

      <div class="col xs12 l7">
        <?php if ( have_rows('latest_news_highlighted') ): ?>
          <div class="latest-news__highlighted">

            <div class="latest-news__carousel carousel" js-latest-news="carousel">
              <?php
              while ( have_rows('latest_news_highlighted') ):
                the_row();
                $post = get_sub_field('post');
                setup_postdata( $post );
              ?>
                <div class="latest-news__card" js-latest-news="cell">
                  <?php if ( has_post_thumbnail() ): ?>
                    <a
                      class="latest-news__card-thumbnail lazyload"
                      data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
                      href="<?php the_permalink(); ?>"
                    >
                    </a>
                  <?php endif; ?>

                  <div class="latest-news__card-body">
                    <a class="latest-news__card-link" href="<?php the_permalink(); ?>">
                      <h4 class="latest-news__card-title"><?php the_title(); ?></h4>
                    </a>

                    <?php if ( get_the_content() ): ?>
                      <p class="latest-news__card-content"><?php echo strip_tags(substr(get_the_content(), 0, 200)); ?>...</p>
                    <?php endif; ?>

                    <p class="latest-news__card-date"><?php the_date('l, jS F Y'); ?></p>
                  </div>
                </div>
              <?php wp_reset_postdata(); endwhile; ?>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
