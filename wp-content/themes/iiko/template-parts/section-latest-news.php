<section
  class="latest-news"
  data-section-type="latest-news"
>
  <div class="latest-news__container container">
    <div class="row">
      <div class="col xs12 l5">
        <?php if ( get_field('latest_news_title') ): ?>
          <h3 class="latest-news__title"><?php the_field('latest_news_title'); ?></h3>
        <?php endif; ?>
        
        
        <ul class="latest-news__posts">
          <?php
          foreach ( get_posts( array( 'posts_per_page' => 3 ) ) as $post ):
            $post_content = get_the_content(null, false, $post);
          ?>
            <li class="latest-news__post">
              <a class="latest-news__post-title" href="<?php the_permalink($post); ?>">
                <?php echo substr(get_the_title($post), 0, 48); if ( strlen(get_the_title($post)) > 48 ): echo '...'; endif; ?>
              </a>
              
              <?php if ( !empty($post_content) ): ?>
              <p class="latest-news__post-excerpt"><?php echo strip_tags(substr($post_content, 0, 140)); ?>...</p>
              <?php endif; ?>
              
              <p class="latest-news__post-date"><?php echo get_the_date('l, jS F Y', $post); ?></p>
            </li>
          <?php endforeach; wp_reset_query(); ?>
        </ul>
            
        <?php if ( get_field('latest_news_button_label') ): ?>
          <a class="button" href="<?php the_field('latest_news_button_link'); ?>">
            <?php the_field('latest_news_button_label'); ?>
          </a>
        <?php endif; ?>
      </div>

      <div class="col xs12 l7 somin">
        <?php if ( have_rows('latest_news_highlighted') ): ?>
          <div class="latest-news__highlighted">
            <div class="latest-news__highlighted-header">
              <span data-mobile="<?php the_field('latest_news_title'); ?>"><?php _e('Highlighted', 'iiko'); ?></span>
            </div>

            <div class="latest-news__carousel carousel" js-latest-news="carousel">
              <?php
              while ( have_rows('latest_news_highlighted') ):
                the_row();
                $post = get_sub_field('post');
                setup_postdata( $post );
              ?>
                <div class="latest-news__card" js-latest-news="cell">
                  <?php if ( has_post_thumbnail() ): ?>
                    <a
                      class="latest-news__card-thumbnail lazyload"
                      data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
                      href="<?php the_permalink(); ?>"
                    >
                    </a>
                  <?php endif; ?>

                  <div class="latest-news__card-body">
                    <a class="latest-news__card-link" href="<?php the_permalink(); ?>">
                      <h4 class="latest-news__card-title"><?php the_title(); ?></h4>
                    </a>

                    <?php if ( get_the_content() ): ?>
                      <p class="latest-news__card-content"><?php echo strip_tags(substr(get_the_content(), 0, 200)); ?>...</p>
                    <?php endif; ?>

                    <p class="latest-news__card-date"><?php the_date('l, jS F Y'); ?></p>
                  </div>
                </div>
              <?php wp_reset_postdata(); endwhile; ?>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
