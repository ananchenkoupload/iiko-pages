<?php if ( has_nav_menu('main-menu') ): ?>
  <ul class="site-nav">
    <?php
    foreach ( get_menu_items('main-menu') as $item ):
      if ( $item->menu_item_parent == 0 ):
        $dropdown = array();
        $has_dropdown = false;
        $item_classes = '';

        foreach ( get_menu_items('main-menu') as $dropdown_item ):
          if ( $dropdown_item->menu_item_parent == $item->ID ):
            array_push($dropdown, $dropdown_item);
          endif;
        endforeach;

        if ( count($dropdown) ):
          $has_dropdown = true;
          $item_classes .= ' site-nav__item--has-dropdown';
        endif;
    ?>
        <li class="site-nav__item<?php echo $item_classes; ?>" js-site-nav="navItem">
          <a class="site-nav__link" href="<?php echo $item->url ?>">
            <?php echo $item->title; ?>
          </a>

          <?php if ( $has_dropdown ): ?>
            <button
              class="site-nav__caret"
              aria-haspopup="true"
              aria-expanded="false"
              js-site-nav="dropdownToggle"
            >
              <svg xmlns="http://www.w3.org/2000/svg" class="icon icon__caret" viewBox="0 0 10 5">
                <path d="M5 5l5-5H0z" fill-rule="evenodd"/>
              </svg>

              <span class="visually-hidden">
                <?php echo __('Toggle the dropdown for ', 'iiko') . '"' . $item->title . '"'; ?>
              </span>
            </button>

            <ul class="site-nav__dropdown" js-site-nav="dropdown">
              <?php foreach ( $dropdown as $dropdown_item ): ?>
                <li class="site-nav__item">
                  <a class="site-nav__link" href="<?php echo $dropdown_item->url; ?>">
                    <?php echo $dropdown_item->title; ?>
                  </a>
                </li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </li>
    <?php
      endif;
    endforeach;
    ?>
  </ul>
<?php endif; ?>
