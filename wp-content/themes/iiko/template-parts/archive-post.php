<article class="card card--archive">
  <div
    class="card__thumbnail lazyload"
    data-bgset="<?php echo get_the_post_thumbnail_url(); ?>"
  >
    <a class="card__link" href="<?php the_permalink(); ?>"></a>
  </div>

  <div class="card__body">
    <a class="card__link" href="<?php the_permalink(); ?>"></a>
    <a class="card__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <p class="card__excerpt"><?php echo substr( get_the_excerpt(), 0, 94 ); ?>...</p>
    
    <div class="card__meta">
      <time datetime="<?php the_date( DATE_W3C ); ?>">
        <?php echo get_the_date('l, jS F Y'); ?>
      </time>
    </div>
  </div>
</article>