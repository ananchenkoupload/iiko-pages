<?php if ( get_field('video_hero_file') ): ?>
  <section
    class="video-hero"
    data-section-type="video-hero"
  >
    <div class="video-hero__player-wrapper">
      <video
        class="video-hero__player"
        autoplay
        js-video-hero="video"
        loop
        muted
        playsinline
      >
        <source src="<?php the_field('video_hero_file'); ?>" type="video/mp4">
      </video>
    </div>

    <button class="video-hero__mute" js-video-hero="mute">
      <svg xmlns="http://www.w3.org/2000/svg" class="icon icon__mute" viewBox="0 0 48 49">
        <defs>
          <path id="a" d="M1230 655h48v49h-48z"/>
        </defs>
        <g transform="translate(-1230 -655)" fill="none" fill-rule="evenodd">
          <image x="1230" y="655" width="48" height="49" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAxCAYAAACcXioiAAAAAXNSR0IArs4c6QAAB4lJREFUaAXNmQmwV1Mcx0tRotUWimpaLFFDQ2pStmxDE08bE7LUTDFFtplMi6hpojQZEyY8icK0kEGhp1QmyxihTeKhRUoLJZTP5//+99/9v3f/7/2398Z35vM/557ld36/c8499777qlfLg5o2PbkJZrpCRzgNWsAxUBfUbvgNNsBqWAFFxcU//0Sak6pn2xunj6Nvf+gH54Ts/El+LWyDP+LldUhPAAM7Ol5m8jnMhEKC+dWCTJVxADh+CoM8CLdCbdgF82ERLMERZzml6G8QnaE7XAv1YB88D+Pp/yNp2ko7AAbW2fvhITDv7D0Ocxh0L2nGwmYtOhXAMDgXtDMeJmDToCpUWgEw0JlYmgWmbo97GGABad7EGNdgbCK0hq+hN2OYlqsKA8BwHyxMB2drDIzD8H7SrIXNI+h8BdSHt7C3Q2OUH04yElzlv2AAda+SplS5AWBwKD0nwSbohbGlKS2lWYFNnZ4HnlpqM5yH7eLYFT+06ULyBhwHw6ibTBqpwyJLKQw5v47LTnlyXoc+AJ1/DjwMGsMQSIixlnDRCbwPJsV9SdSHM5EB0KGvHcH93hmDG0lzEjZ9VuiYR+6j2LyDdBooj9iEaFuDi9FQG/4Gg+hNWkZlAqBhW1q55902lzNQVudzeCRstuJ6ObSB4dgcEa4P5+POF1LWD94E/dGHF6jzEElSUgA0OJJaTxtvJvf8RtKchM32GFgGJ8Ft2PTojVSE8wW0dxdcD/o0izauSkJJAVDq3X8GjKZjPm5Yb8Yi8MZ1QlzZSKVwfr+N6efWGw+ugM+ihBKnEAZaUPoNbIB2dHLvZS3sXUXn1+MGemBvYWljtGlA2Q6YAU5msG2c+ZjzlMVEW1fgKzgV2lD/oxXhFXiA61rgsZWr8z475oJn+UVRzlMeljdoSudtGPfJ2XcLuVNiiq0A0XmUGdEXNDy/pCq7X2wNoudTsBW6Y89ZixRtG1KxPV7pDVtm5kt3pM9KyryxT8X21mAFbqbAJZoIWQnDNcCZeRp+gC4VOO9RORXUCqjQ+VjLatWmkLoK/b2u6Q+6EX4H3yojhXODqRgFx0Y2OFS4iuyVOP/ToaLknMFSUghuG7WG9kl7vqQ48nc2pQbeFybWxFgzMmfBdIy4Z8uINh0otJPb7CU4AFEqpnAydn6LqrSslPMfUnRRqrZR5fqIjbeo60fa1BXoFm9Y5pSIl5tcEM8PxMA7ofKMsqWcd8/fDlsyMlLS+G0SV6+r90Dg3EcldZG/Lrny3SQrRThfgKF0t03pMb1nVCcD8PG+k5n9xZLKUJTzjJet8x6p3+HnTmhjAK1gHVSK8u18yMmN5FsagE9Dz+y8qxKd11dPuQYGUAf2QF5Vyc7r6z9QzwDUwZIkP79V4LyOxl53DGAvHGVJPlRFzutqXdhtAN7Nx0POqkLn9bUR7DQAT6CWkJOq2Hl99fhfbwBroREO+EaalaraecY7GUf9orfWVwmfareBfz29BlHywaHuo3Nz0tI3/dWU+WT11fmGXB5S9E9H+qqWG8Bic+hSSBXAAurmQE+4ClLJl8LnCPIWgvg3VaM8lF8Wt1F0GAOt52IN9GRg/yYoI9ocgOuoaA++PYoBrwLli5lGfdG7CeZgyw8EeRd2/arnRK7Cp++9B9QM8KNTebPrO8iXsJh2S2AA+JdR8JfUIvI9wK8a18ACBvOP+XzL7doQZmo4COBF8i75vRaWJ5zyzbQQfJ0NnI+9mBGc6Y3wDLhKi2jvxORTwzHmQ0wfSgJg4GLyL0MXBuxGGqnynA86YMuJGATjoQMU0a8Jac7CjpPSCWYwzs8aDFbA/Dj4B/yMFy63rvRfUkkzH2sQ+sH4QXiIogfgdFiKzZahJhln6V+TTpPB2X8sMJBwlAFXxxt4oyZtJTqn3DaBoagUmxMovxOawjLsnB3VLs2yobSz/xPYXR/0SQQQLxhD6oetsQx2jmXZOm9fxWDPkvSFBrAEe26BjESfc+ngrH8HY8OdkwJgsN1U9oHqMJeO7t1C6AflbhvqUwq7s6n0ZPKY9sa+ImXjUhW0PZGi4Auf/7XZE26SFIAVNFhJMgRc9m8hJ+fpHxN23yVzKbiH5+OYq9Ia1J8lSfIvbTyGfbY0g8HY+Iw0Sc50pOg8kopRsA8S3yLJ5yTsuo8XQvAG7CeaC3Hu47Bh2jXmWufbwSjqR4frg7w3Z6R27dpdVL9+PQN01grIL6Zsc2TjDAqxsQVbbomasBruxrmlYRM470HyHpwGfimPdN4+KVfASoWxgSRTwfN9BHgKpPqwRXX2Yiy39F3g6WV+CGNNI02pCgOwJ4bPJ3kFmsOnMBzDRaR5E2N4Ok0BT5zvoS9jfEJartIKQAsM4Pv3KLgb3Hrvw5OwINsVwaZ2uoOvBxeDq2wQ7vldpBUq7QACSwzqk/Vh6AU6sAnmwduwnIG3kaYU/X0edINLoAC8WXXco/YR+nvypa2MAwgs40hz8gPAY7ZFUE5aDD+A7yr7QB0Nx0IrOAkC+dCcCX5YdttkrKwDCI9EMG257godwZPDgBpBWNu50GFPnhVQhNOrSP+/IrC6Upke/gfjyL3jCQYLPAAAAABJRU5ErkJggg=="/>
          <use fill-opacity="0" fill="#000" xlink:href="#a"/>
        </g>
      </svg>
    </button>
  </section>
<?php endif; ?>
