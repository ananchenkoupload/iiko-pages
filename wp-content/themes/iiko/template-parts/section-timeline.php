<section
  class="timeline"
  data-section-type="timeline"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <div class="timeline__header">
          <?php if ( get_field('timeline_title') ): ?>
            <h3 class="timeline__title"><?php the_field('timeline_title'); ?></h3>
          <?php endif; ?>

          <?php if ( get_field('timeline_download') ): ?>
            <a
              class="timeline__download"
              download
              href="<?php the_field('timeline_download'); ?>"
            >
              <?php _e('Download here', 'iiko'); ?>

              <img
                class="timeline__download-icon"
                alt="<?php _e('Download ', 'iiko'); ?>"
                src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-download.png"
              >
            </a>
          <?php endif; ?>
        </div>

        <?php if ( have_rows('timeline_items') ): ?>
          <div class="timeline__items">
            <?php while( have_rows('timeline_items') ): the_row(); ?>
              <div class="timeline__item"><?php the_sub_field('content'); ?></div>
            <?php endwhile; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>