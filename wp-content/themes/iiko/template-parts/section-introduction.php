<section
  class="introduction"
  data-section-type="introduction"
>
  <div class="introduction__header">
    <div class="container">
      <div class="row">
        <div class="col xs12">
          <?php if ( get_field('introduction_header_image') ): ?>
            <img
              class="introduction__header-image"
              alt="<?php echo get_field('introduction_header_image')['alt']; ?>"
              src="<?php echo get_field('introduction_header_image')['url']; ?>"
            >
          <?php else: ?>
            <div class="introduction__title">
              <div class="introduction__title-part">
                <span class="introduction__heading"><?php the_field('introduction_title_part_1'); ?></span>
                <span class="introduction__heading"><?php the_field('introduction_title_part_2'); ?></span>
              </div>
              <div class="introduction__title-part">
                <span class="introduction__heading"><?php the_field('introduction_title_part_3'); ?></span>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>

  <?php if ( get_field('introduction_content') || get_field('introduction_image_1') || get_field('introduction_image_2') || get_field('introduction_image_3') ): ?>
    <div class="introduction__body">
      <div class="container">
        <div class="row">
          <div class="col xs12 l5">
            <div class="introduction__content">
              <?php if ( get_field('introduction_content') ): ?>
                <p><?php the_field('introduction_content'); ?></p>
              <?php endif; ?>
            </div>
          </div>

          <div class="col xs12 l7">
            <div class="introduction__images">
              <?php if ( get_field('introduction_image_1') ): ?>
                <div
                  class="introduction__image introduction__image--1 lazyload"
                  data-bgset="<?php the_field('introduction_image_1'); ?>"
                >
                </div>
              <?php endif; ?>

              <?php if ( get_field('introduction_image_2') ): ?>
                <div
                  class="introduction__image introduction__image--2 lazyload"
                  data-bgset="<?php the_field('introduction_image_2'); ?>"
                >
                </div>
              <?php endif; ?>

              <?php if ( get_field('introduction_image_3') ): ?>
                <div
                  class="introduction__image introduction__image--3 lazyload"
                  data-bgset="<?php the_field('introduction_image_3'); ?>"
                >
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
</section>