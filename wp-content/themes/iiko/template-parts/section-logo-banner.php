<?php if ( have_rows('logo_banner_logos') ): ?>
  <section
    id="LogoBanner"
    class="logo-banner"
    data-section-type="logo-banner"
  >
    <div class="container">
      <div class="row">
        <div class="col xs12">
          <div class="logo-banner__container">
            <div class="logo-banner__logos">
              <?php
              while ( have_rows('logo_banner_logos') ): the_row();
                $image = get_sub_field('logo');
                $alt_tag = $image['alt'];
                $url = $image['url'];
              ?>
                <img
                  class="logo-banner__logo lazyload"
                  alt="<?php echo $alt_tag; ?>"
                  data-src="<?php echo $url; ?>"
                >
              <?php endwhile; ?>
            </div>

            <a class="logo-banner__anchor" href="#LogoBanner">
              <img
                class="logo-banner__anchor-icon"
                src="<?php echo get_template_directory_uri(); ?>/assets/images/white-arrow.png"
              >
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>