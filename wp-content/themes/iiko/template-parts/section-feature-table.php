<section
  class="feature-table"
  data-section-type="feature-table"
>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <?php if ( get_field('feature_table_title') ): ?>
          <h3 class="feature-table__title"><?php the_field('feature_table_title'); ?></h3>
        <?php endif; ?>

        <table class="feature-table__table">
          <thead>
            <th></th>
            <?php while ( have_rows('feature_table_columns') ): the_row(); ?>
              <th><span><?php the_sub_field('title'); ?></span></th>
            <?php endwhile; ?>
            <th></th>
          </thead>
          <tbody>
            <?php
            while ( have_rows('feature_table_rows') ): the_row();
              $parent_row = get_row_index();
            ?>
              <tr js-feature-table="row" data-row="<?php echo $parent_row; ?>">
                <td><?php the_sub_field('title'); ?></td>
                <?php
                $column_max = count( get_field('feature_table_columns') );

                while ( have_rows('column_values') ): the_row();
                  if ( get_row_index() <= $column_max ):
                ?>
                    <td>
                      <?php if ( get_sub_field('value') === 'half_circle' ): ?>
                        <span class="feature-table__circle feature-table__circle--half"></span>
                      <?php elseif ( get_sub_field('value') === 'full_circle' ): ?>
                        <span class="feature-table__circle"></span>
                      <?php endif; ?>
                    </td>
                <?php
                  endif;
                endwhile;
                ?>
                <td>
                  <?php if ( have_rows('sub_rows') ): ?>
                    <button class="feature-table__row-toggle" js-feature-table="rowToggle">
                      <span class="visually-hidden"><?php _e('Open/close sub rows', 'iiko'); ?></span>
                      <svg viewBox="0 0 36 36" class="icon icon__chevron" xmlns="http://www.w3.org/2000/svg">
                        <path d="M12.746 32l-3.184-3.183L20.07 18.495 9.562 8.184 12.746 5l13.691 13.495z" fill="#000" fill-rule="evenodd"/>
                      </svg>
                    </button>
                  <?php endif; ?>
                </td>
              </tr>
              <?php
              if ( have_rows('sub_rows') ):
                while ( have_rows('sub_rows') ): the_row();
              ?>
                  <tr
                    class="feature-table__sub-row"
                    js-feature-table="row"
                    data-row="<?php echo $parent_row; ?>"
                  >
                    <td><?php the_sub_field('title'); ?></td>
                    <?php
                    $column_max = count( get_field('feature_table_columns') );

                    while ( have_rows('column_values') ): the_row();
                      if ( get_row_index() <= $column_max ):
                    ?>
                        <td>
                          <?php if ( get_sub_field('text') ): ?>
                            <span><?php the_sub_field('text'); ?></span>
                          <?php elseif ( get_sub_field('check') ): ?>
                          <svg viewBox="0 0 36 36" class="icon icon__tick" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15 27.035l-9-8.638 2.791-2.87 6.156 5.874 12.21-12.436L30 11.782z" fill="#129E3A" fill-rule="evenodd"/>
                          </svg>
                          <?php endif; ?>
                        </td>
                    <?php
                      endif;
                    endwhile;
                    ?>
                    <td></td>
                  </tr>
              <?php
                endwhile;
              endif;
              ?>

            <?php endwhile; ?>
          </tbody>
        </table>

        <div class="feature-table__footer">
          <?php if ( get_field('feature_table_footer_title') ): ?>
            <h4 class="feature-table__footer-title"><?php the_field('feature_table_footer_title'); ?></h4>
          <?php endif; ?>

          <?php if ( get_field('feature_table_footer_subtitle') ): ?>
            <h5 class="feature-table__footer-subtitle"><?php the_field('feature_table_footer_subtitle'); ?></h5>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
