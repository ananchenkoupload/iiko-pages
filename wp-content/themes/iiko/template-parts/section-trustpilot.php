<section
  class="trustpilot"
  data-section-type="trustpilot"
>
  <div class="trustpilot__body">
    <div class="container">
      <div class="row align-center">
        <div class="col xs12 l5">
          <?php if ( get_field('trustpilot_header_image') ): ?>
            <img
              class="trustpilot__header-image"
              alt="<?php the_field('trustpilot_title'); ?>"
              src="<?php the_field('trustpilot_header_image'); ?>"
            >
          <?php endif; ?>

          <?php if ( get_field('trustpilot_title') ): ?>
            <h3 class="trustpilot__title"><?php the_field('trustpilot_title'); ?></h3>
          <?php endif; ?>

          <?php if ( get_field('trustpilot_content') ): ?>
            <p class="trustpilot__content"><?php the_field('trustpilot_content'); ?></p>
          <?php endif; ?>

          <?php if ( have_rows('trustpilot_logos') ): ?>
            <div class="trustpilot__logos">
              <?php while ( have_rows('trustpilot_logos') ): the_row(); ?>
                <img
                  class="trustpilot__logo"
                  alt="<?php _e('Trustpilot client logo', 'iiko'); ?>"
                  src="<?php the_sub_field('logo'); ?>"
                >
              <?php endwhile; ?>
            </div>
          <?php endif; ?>
        </div>

        <div class="col xs12 l7">
          <?php if ( get_field('trustpilot_main_image') ): ?>
            <img
              class="trustpilot__main-image"
              alt="<?php the_field('trustpilot_title'); ?>"
              src="<?php the_field('trustpilot_main_image'); ?>"
            >
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>