<section
  class="reviews"
  data-section-type="review"
>
  <div class="container">
    <div class="row">
      <?php while ( have_rows('reviews') ): the_row(); ?>
        <div class="col xs12 l6">
          <div class="reviews__item">
            <?php if ( get_sub_field('content') ): ?>
              <p class="reviews__item-content"><?php the_sub_field('content'); ?></p>
            <?php endif; ?>

            <?php if ( get_sub_field('name') ): ?>
              <p class="reviews__item-name"><?php the_sub_field('name'); ?></p>
            <?php endif; ?>

            <?php if ( get_sub_field('position') ): ?>
              <p class="reviews__item-position"><?php the_sub_field('position'); ?></p>
            <?php endif; ?>

            <?php if ( get_sub_field('stars') ): ?>
              <ul class="reviews__item-stars">
                <?php for ( $i = 1; $i <= get_sub_field('stars'); $i++): ?>
                  <li class="reviews__item-star">
                    <span class="visually-hidden"><?php _e('Star', 'iiko'); ?></span>
                  </li>
                <?php endfor; ?>
              </ul>
            <?php endif; ?>

            <?php if ( get_sub_field('call_to_action_label') ): ?>
              <a class="reviews__item-cta" href="<?php the_sub_field('call_to_action_link'); ?>">
                <?php the_sub_field('call_to_action_label'); ?>
              </a>
            <?php endif; ?>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  </div>
</section>