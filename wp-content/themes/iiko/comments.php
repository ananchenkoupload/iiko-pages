<?php
/**
 * Template for displaying comments.
 */
?>
<div class="comments">
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <?php
        /**
         * If comments belong to the post.
         */
        if ( have_comments() ): ?>
          <h3 class="comments__title"><?php _e('Comments', 'iiko'); ?></h3>
          <ol class="comments__list">
            <?php
            /**
             * Loop through and list the comments.
             */
            wp_list_comments();
            ?>
          </ol>

          <?php
          /**
           * Check if there are more than one comments page.
           */
          if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ):
          ?>
            <div class="comments__nav">
              <div class="comments__nav-link"><?php previous_comments_link( __('&larr; Older Comments', 'iiko') ); ?></div>
              <div class="comments__nav-link"><?php next_comments_link( __('Newer Comments &rarr;', 'iiko') ); ?></div>
            </div>
          <?php endif; ?>
        <?php endif; ?>

        <?php
        /**
         * If comments are closed, leave a note.
         * Only display if comments already exist.
         */
        if ( !comments_open() && have_comments() ):
        ?>
          <div class="comments__closed"><?php _e('Comments are closed.', 'iiko'); ?>
        <?php endif; ?>

        <?php
        /**
         * Outputs the comment form.
         */
        comment_form();
        ?>
      </div>
    </div>
  </div>
</div>