<?php get_header() ?>
  <div class="container">
    <div class="row">
      <div class="col xs12">
        <h1 class="page-not-found"><?php _e('404 - Page not found', 'iiko'); ?></h1>
      </div>
    </div>
  </div>
<?php get_footer() ?>