<?php get_header(); ?>
  <?php
  get_template_part('template-parts/section', 'video-hero');
  get_template_part('template-parts/section', 'logo-banner');
  get_template_part('template-parts/section', 'introduction');
  get_template_part('template-parts/section', 'features');
  get_template_part('template-parts/section', 'testimonials');
  get_template_part('template-parts/section', 'latest-news');
  get_template_part('template-parts/section', 'trustpilot');
  ?>
<?php get_footer(); ?>