<?php
/**
 * Template for displaying search results.
 */

get_header();
?>
  <header class="archive__header">
    <div class="container">
      <div class="row">
        <div class="col xs12">
          <h1 class="archive__title">
            <?php
            _e('Search results for: ', 'iiko');
            the_search_query();
            ?>
          </h1>
        </div>
      </div>
    </div>
  </header>

  <div class="archive__body">
    <?php if ( have_posts() ): ?>
      <div class="container">
        <div class="row">
          <div class="col xs12">
            <div class="row">
              <?php
              /**
               * Start the loop.
               */
              while ( have_posts() ): the_post();
              ?>
                <div class="col xs6 m4 l3">
                  <?php get_template_part('template-parts/archive', 'post'); ?>
                </div>
              <?php endwhile; ?>

              <div class="col xs12">
                <nav class="archive__pagination pagination" role="navigation">
                  <?php
                  /**
                   * Display the pagination.
                   */
                  echo paginate_links(array(
                    'next_text' => __('Next', 'iiko'),
                    'prev_text' => __('Previous', 'iiko')
                  ));
                  ?>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php else: ?>
      <div class="container">
        <div class="row">
          <div class="col xs12">
            <h1 class="no-content"><?php _e('Sorry, no posts matched your criteria.', 'iiko'); ?></h1>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
<?php get_footer(); ?>