<?php
/**
 * Template Name: Flexible Content
 */
get_header();


// Main loop
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		$modules = get_field( 'modules' );

		if ( is_array( $modules ) ) {
			foreach( $modules as $module ) {

				switch ( $module['acf_fc_layout'] ) {
					case 'tall_hero':
						include( locate_template( 'modules/tall-hero.php', false, false ) );
						break;
					case 'media_long_content':
						include( locate_template( 'modules/media-long-content.php', false, false ) );
						break;
					case 'cta_module':
						include( locate_template( 'modules/cta-module.php', false, false ) );
						break;
					case 'video_product':
						include( locate_template( 'modules/video-product.php', false, false ) );
						break;
					case 'content_with_media':
						include( locate_template( 'modules/content-with-media.php', false, false ) );
						break;
					case 'landing_trust_pilot':
						include( locate_template( 'modules/landing-trustpilot.php', false, false ) );
						break;
					case 'accordian':
						include( locate_template( 'modules/accordion.php', false, false ) );
						break;
					case 'product_hero':
						include( locate_template( 'modules/product-hero.php', false, false ) );
						break;
					case 'center_trust_pilot':
						include( locate_template( 'modules/center-trustpilot.php', false, false ) );
						break;
					case 'latest_blog_slider':
						include( locate_template( 'modules/latest-news-product.php', false, false ) );
						break;	
					default:
						if ( current_user_can( 'manage_options' ) ) { echo 'Module not found: ' . $module['acf_fc_layout']; }
				}
			}
		} else {
			if ( current_user_can( 'manage_options' ) ) {
				echo 'No modules were found';
			}
		}
	}
}

get_footer();
get_footer();
?>