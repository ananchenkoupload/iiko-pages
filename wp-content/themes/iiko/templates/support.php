<?php
/**
 * Template Name: Support
 */
get_header();
  get_template_part('template-parts/section', 'introduction');
  get_template_part('template-parts/section', 'testimonials');
  get_template_part('template-parts/section', 'image-with-text');
  get_template_part('template-parts/section', 'featured-reviews');
get_footer();
?>