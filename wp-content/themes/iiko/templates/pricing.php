<?php
/**
 * Template Name: Pricing
 */
get_header();
  get_template_part('template-parts/section', 'introduction');
  get_template_part('template-parts/section', 'pricing-plans');
  get_template_part('template-parts/section', 'feature-table');
  get_template_part('template-parts/section', 'reviews');
get_footer();
?>