<?php
/**
 * Template Name: Solution
 */
get_header();
  get_template_part('template-parts/section', 'introduction');
  get_template_part('template-parts/section', 'video');
  get_template_part('template-parts/section', 'feature-toggle');
  get_template_part('template-parts/section', 'feature-tabs');
  get_template_part('template-parts/section', 'features');
get_footer();
?>