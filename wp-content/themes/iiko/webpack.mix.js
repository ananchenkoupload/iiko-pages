/**
 * Mix File
 * ------------------------------------------------------------------------------
 * Contains tasks and functions to build a theme package.
 * ------------------------------------------------------------------------------
 *
 */
let mix = require('laravel-mix');
let dotenv = require('dotenv');

/**
 * dotenv configuration.
 */
dotenv.config();

/**
 * Global variables.
 */
const proxy = process.env.LOCAL_URL || 'iiko.local';
const enableProxy = process.env.ENABLE_PROXY || 'true';

/**
 * General Mix settings.
 */
mix.sourceMaps();
mix.extract();

/**
 * Scripts configuration.
 */
mix.js('assets/scripts/layout/theme.js', 'assets/scripts/theme.js');

/**
 * Styles configuration.
 */
mix.sass('assets/styles/theme.scss', 'assets/styles/theme.css')
  .options({
    autoprefixer: {
      options: {
        browsers: ['last 6 versions'],
      },
    },
    processCssUrls: false,
  });


/**
 * BrowserSync configuration.
 */
if (enableProxy === 'true') {
  mix.browserSync({
    proxy: proxy,
    files: [
      'assets/scripts/theme.js',
      'assets/styles/theme.css',
      '**/*.php',
    ],
    snippetOptions: {
      ignorePaths: 'wp-admin/**',
    },
  });
}
