<?php
/**
 * Single page template.
 */

get_header(); ?>
  <?php
  if( have_posts() ):
    /**
     * Start the loop.
     */
    while( have_posts() ): the_post();

      get_template_part( 'template-parts/page', 'content' );

      if ( comments_open() || get_comments_number() ):
        comments_template();
      endif;
    
    endwhile;
  else:
  ?>
    <div class="container">
      <div class="row">
        <div class="col s12">
          <h1 class="no-content">
            <?php _e('Sorry, no pages matched your criteria.', 'iiko'); ?>
          </h1>
        </div>
      </div>
    </div>
  <?php endif; ?>
<?php get_footer(); ?>