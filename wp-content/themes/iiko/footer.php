    </main>

    <?php
    if ( get_page_template_slug() !== 'templates/success.php'):
      get_template_part('template-parts/section', 'newsletter');
    endif;
    ?>

    <footer class="site-footer" role="contentinfo">
      <div class="container">
        <div class="row">
          <div class="col xs12">
            <div class="site-footer__main">
              <div class="row align-end">
                <div class="col xs12 l8">
                  <?php if ( get_field('footer_logo', 'option') ): ?>
                    <img
                      class="site-footer__logo lazyload"
                      alt="<?php echo get_bloginfo('name'); ?>"
                      data-src="<?php the_field('footer_logo', 'option'); ?>"
                    >
                  <?php endif; ?>

                  <div class="site-footer__nav-wrapper">
                    <?php if ( has_nav_menu('footer-menu-1') ): ?>
                      <nav class="site-footer__nav" role="navigation">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-1' ) ); ?>
                      </nav>
                    <?php endif; ?>

                    <?php if ( has_nav_menu('footer-menu-2') ): ?>
                      <nav class="site-footer__nav" role="navigation">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-2' ) ); ?>
                      </nav>
                    <?php endif; ?>

                    <?php if ( has_nav_menu('footer-menu-3') ): ?>
                      <nav class="site-footer__nav" role="navigation">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-3' ) ); ?>
                      </nav>
                    <?php endif; ?>

                    <?php if ( has_nav_menu('footer-menu-4') ): ?>
                      <nav class="site-footer__nav" role="navigation">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-4' ) ); ?>
                      </nav>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="col xs12 l4">
                  <?php if ( get_field('footer_phone_label', 'option') ): ?>
                    <p class="site-footer__phone-label"><?php the_field('footer_phone_label', 'option'); ?></p>
                  <?php endif; ?>

                  <?php if ( get_field('footer_phone', 'option') ): ?>
                    <a class="site-footer__phone" href="tel:<?php the_field('footer_phone', 'option'); ?>">
                      <?php the_field('footer_phone', 'option'); ?>
                    </a>
                  <?php endif; ?>
                </div>
              </div>
            </div>

            <div class="site-footer__additional">
              <div class="row">
                <div class="col xs12 s6">
                  <div class="site-footer__socials">
                    <?php if ( get_field('footer_facebook', 'option') ): ?>
                      <a class="site-footer__social" href="<?php the_field('footer_facebook', 'option'); ?>">
                        <span class="visually-hidden"><?php _e('Facebook', 'iiko'); ?></span>
                        <svg viewBox="0 0 24 24" class="icon icon__facebook" xmlns="http://www.w3.org/2000/svg">
                          <path d="M22.675 0H1.325C.593 0 0 .593 0 1.325v21.351C0 23.407.593 24 1.325 24H12.82v-9.294H9.692v-3.622h3.128V8.413c0-3.1 1.893-4.788 4.659-4.788 1.325 0 2.463.099 2.795.143v3.24l-1.918.001c-1.504 0-1.795.715-1.795 1.763v2.313h3.587l-.467 3.622h-3.12V24h6.116c.73 0 1.323-.593 1.323-1.325V1.325C24 .593 23.407 0 22.675 0z"/>
                        </svg>
                      </a>
                    <?php endif; ?>

                    <?php if ( get_field('footer_linkedin', 'option') ): ?>
                      <a class="site-footer__social" href="<?php the_field('footer_linkedin', 'option'); ?>">
                        <span class="visually-hidden"><?php _e('LinkedIn', 'iiko'); ?></span>
                        <svg viewBox="0 0 24 24" class="icon icon__linkedin" xmlns="http://www.w3.org/2000/svg">
                          <path d="M4.98 3.5C4.98 4.881 3.87 6 2.5 6S.02 4.881.02 3.5C.02 2.12 1.13 1 2.5 1s2.48 1.12 2.48 2.5zM5 8H0v16h5V8zm7.982 0H8.014v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0V24H24V13.869c0-7.88-8.922-7.593-11.018-3.714V8z"/>
                        </svg>
                      </a>
                    <?php endif; ?>

                    <?php if ( get_field('footer_youtube', 'option') ): ?>
                      <a class="site-footer__social" href="<?php the_field('footer_youtube', 'option'); ?>">
                      <span class="visually-hidden"><?php _e('YouTube', 'iiko'); ?></span>
                      <svg viewBox="0 0 24 24" class="icon icon__youtube" xmlns="http://www.w3.org/2000/svg">
                        <path d="M19.615 3.184c-3.604-.246-11.631-.245-15.23 0C.488 3.45.029 5.804 0 12c.029 6.185.484 8.549 4.385 8.816 3.6.245 11.626.246 15.23 0C23.512 20.55 23.971 18.196 24 12c-.029-6.185-.484-8.549-4.385-8.816zM9 16V8l8 3.993L9 16z"/>
                      </svg>
                      </a>
                    <?php endif; ?>

                    <?php if ( get_field('footer_twitter', 'option') ): ?>
                      <a class="site-footer__social" href="<?php the_field('footer_twitter', 'option'); ?>">
                      <span class="visually-hidden"><?php _e('Twitter', 'iiko'); ?></span>
                        <svg viewBox="0 0 24 24" class="icon icon__twitter" xmlns="http://www.w3.org/2000/svg">
                          <path d="M24 4.557a9.83 9.83 0 0 1-2.828.775 4.932 4.932 0 0 0 2.165-2.724 9.864 9.864 0 0 1-3.127 1.195 4.916 4.916 0 0 0-3.594-1.555c-3.179 0-5.515 2.966-4.797 6.045A13.978 13.978 0 0 1 1.671 3.149a4.93 4.93 0 0 0 1.523 6.574 4.903 4.903 0 0 1-2.229-.616c-.054 2.281 1.581 4.415 3.949 4.89a4.935 4.935 0 0 1-2.224.084 4.928 4.928 0 0 0 4.6 3.419A9.9 9.9 0 0 1 0 19.54a13.94 13.94 0 0 0 7.548 2.212c9.142 0 14.307-7.721 13.995-14.646A10.025 10.025 0 0 0 24 4.557z"/>
                        </svg>
                      </a>
                    <?php endif; ?>
                  </div>
                </div>

                <div class="col xs12 s6">
                  <?php if ( get_field('footer_copy', 'option') ): ?>
                    <div class="site-footer__copy"><?php the_field('footer_copy', 'option'); ?></div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
   <script>
     var accItem = document.getElementsByClassName('accordionItem');
    var accHD = document.getElementsByClassName('accordionItemHeading');
    for (i = 0; i < accHD.length; i++) {
        accHD[i].addEventListener('click', toggleItem, false);
    }
    function toggleItem() {
        var itemClass = this.parentNode.className;
        for (i = 0; i < accItem.length; i++) {
            accItem[i].className = 'accordionItem close';
        }
        if (itemClass == 'accordionItem close') {
            this.parentNode.className = 'accordionItem open';
        }
    }
    </script>

    <?php wp_footer(); ?>
	</body>
</html>
