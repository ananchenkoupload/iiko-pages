/**
 * Helper: Event Bus
 * ------------------------------------------------------------------------------
 * A global tool for communicating between components.
 *
 * @namespace eventBus
 */
export default () => {

  /**
   * Create a new Event Bus object.
   */
  const eventBus = {};

  /**
   * Adds a handler function to each event
   * ready to be called.
   * @param {String|Array} events - The event or events.
   * @param {Function} handler - The function to execute when event is emited.
   */
  function listen(events, handler) {
    [...[].concat(events)].forEach((event) => {
      eventBus[event] = (eventBus[event] || []).concat(handler);
    });
  }

  /**
   * Emit an event from the bus.
   * @param {String|Array} events - The event or events to emit.
   * @param {*} data - Data to send to listeners.
   */
  function emit(events, data) {
    [...[].concat(events)].forEach((event) => {
      if (!eventBus[event]) {
        return;
      }

      return eventBus[event].forEach((handler) => handler(data));
    });
  }

  /**
   * Returns all events.
   */
  function all() {
    return eventBus;
  }
  
  return {
    all,
    emit,
    listen,
  }
}
