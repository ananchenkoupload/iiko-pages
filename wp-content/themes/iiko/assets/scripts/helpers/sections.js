/**
 * Global utilities.
 */
window.Theme = window.Theme || {};
window.Theme.sections = window.Theme.sections || {};

export var registered = (window.Theme.sections.registered = window.Theme.sections.registered || {});
export var instances = (window.Theme.sections.instances = window.Theme.sections.instances || []);

/**
 * Register a theme section.
 * @param {String} type - The section type, i.e. image-slider.
 * @param {Object} properties - The section properties.
 */
export function register(type, properties) {
  if (typeof type !== 'string') {
    throw new TypeError(
      'Theme Sections: The first argument for .register must be a string that specifies the type of the section being registered'
    );
  }

  if (typeof registered[type] !== 'undefined') {
    throw new Error(
      'Theme Sections: A section of type "' +
        type +
        '" has already been registered. You cannot register the same section type twice'
    );
  }

  const selector = `[data-section-type="${type}"]`;

  function TypedSection(container) {
    container = {
      container,
      selector,
      type,
    };

    return Object.assign(container, properties);
  }

  const containers = [...document.querySelectorAll(`[data-section-type="${type}"]`)];

  containers.forEach((container) => {
    instances.push(new TypedSection(container));
  });

  return registered[type] = properties;
}

/**
 * Load a section.
 * @param {String|Array} types - Section types.
 */
export function load(types) {
  types = normalizeType(types);

  types.forEach((type) => {
    const section = registered[type];
    const sectionInstances = instances.filter((instance) => instance.type === type);

    if (section === 'undefined') {
      return;
    }

    sectionInstances.forEach((section) => {
      section.onLoad();
    });
  });
}

/**
 * Normalize the type input.
 * @param {String|Array} types - The types.
 */
function normalizeType(types) {
  if (types === '*') {
    types = Object.keys(registered);
  } else if (typeof types === 'string') {
    types = [types];
  } else if (Array.isArray(types)) {
    // Does not currently support arrays.
  }

  return types;
}