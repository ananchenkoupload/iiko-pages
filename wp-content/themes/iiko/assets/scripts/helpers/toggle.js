/**
 * Toggle
 * ------------------------------------------------------------------------------
 * A simple helper to toggle elements with an active class.
 * - Toggle must have a `js-toggle` attribute matching the target's ID.
 *
 * @namespace toggle
 */
import {enableBodyScroll, disableBodyScroll} from 'body-scroll-lock';
import cssClasses from '../helpers/css-classes';
import {on, extendDefaults} from '../helpers/utils';
import * as a11y from '../helpers/a11y';

/**
 * Create a new toggle instance.
 */
export default (config) => {

  /**
   * Defaults.
   */
  const defaults = {
    namespace: config.namespace,
    lockScroll: true,
    overlay: false,
    trapFocus: true,
  }

  /**
   * Settings.
   */
  const settings = extendDefaults(defaults, config);

  /**
   * Node selectors.
   */
  const toggle = document.querySelectorAll(`[js-toggle="${settings.namespace}"]`);
  const target = document.getElementById(settings.namespace);

  /**
   * Global instances.
   */
  const focusTrap = a11y.createFocusTrap(target);

  /**
   * Initiate the instance.
   */
  function init() {
    setClickEvents();
    setListeners();
    setTargetDefaults();
  }

  /**
   * Set the target default state.
   */
  function setTargetDefaults() {
    if (targetIsOpen()) {
      a11y.addToTree(target);
      return;
    }
    
    a11y.hideFromTree(target);
  }

  /**
   * Set click events.
   */
  function setClickEvents() {
    [...toggle].forEach((element) => {
      on('click', element, (event) => handleToggleClick(event));
    });

    Theme.EventBus.listen('Overlay:on:click', () => closeTarget());
    Theme.EventBus.listen(`Toggle:${settings.namespace}:close`, () => closeTarget());
    Theme.EventBus.listen(`Toggle:${settings.namespace}:open`, () => openTarget());
  }

  /**
   * Set listeners.
   */
  function setListeners() {
    Theme.EventBus.listen(`EscEvent:${settings.namespace}:on`, () => closeTarget());
  }

  /**
   * Handle the toggle click event.
   * @param {Object} event - The event object.
   */
  function handleToggleClick(event) {
    event.preventDefault();
    toggleTarget();
  }

  /**
   * Toggle the target.
   */
  function toggleTarget() {
    return targetIsOpen() ? closeTarget() : openTarget();
  }

  /**
   * Returns if the target is open.
   */
  function targetIsOpen() {
    return target.classList.contains(cssClasses.active);
  }

  /**
   * Open the target.
   */
  function openTarget() {
    target.classList.add(cssClasses.active);

    a11y.escEvent(target, 'on', settings.namespace);
    a11y.addToTree(target);

    if (settings.lockScroll) {
      disableBodyScroll(target);
    }

    if (settings.overlay) {
      Theme.EventBus.emit('Overlay:show');
    }

    if (settings.trapFocus) {
      focusTrap.activate();
    }
  }

  /**
   * Close the target.
   */
  function closeTarget() {
    target.classList.remove(cssClasses.active);

    a11y.escEvent(target, 'off');
    a11y.hideFromTree(target);

    if (settings.lockScroll) {
      enableBodyScroll(target);
    }

    if (settings.overlay) {
      Theme.EventBus.emit('Overlay:hide');
    }

    if (settings.trapFocus) {
      focusTrap.deactivate();
    }
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
}