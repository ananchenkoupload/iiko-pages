/**
 * Helper: CSS Classes
 * ------------------------------------------------------------------------------
 * A collection of CSS classes for consistency.
 *
 * @namespace cssClasses
 */
export default {
  active: 'is-active',
  closed: 'is-closed',
  collapsed: 'is-collapsed',
  detached: 'is-detached',
  disabled: 'is-disabled',
  fixed: 'is-fixed',
  hidden: 'is-hidden',
  open: 'is-open',
  sticky: 'is-sticky',
};
