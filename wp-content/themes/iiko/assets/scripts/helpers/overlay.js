/**
 * Helper: Overlay
 * ------------------------------------------------------------------------------
 * A reusable window overlay for toggleable elements.
 *
 * @namespace overlay
 */
import cssClasses from '../helpers/css-classes';
import {on} from '../helpers/utils';

/**
 * Create a new overlay instance.
 */
export default () => {

  /**
   * Selectors.
   */
  const selectors = {
    container: '.overlay',
  };

  /**
   * Initiate the overlay.
   */
  function init() {
    setListeners();

    if (!hasOverlay()) {
      buildOverlay();
    }

    setClickEvents();
  }

  /**
   * Set event listeners.
   */
  function setListeners() {
    Theme.EventBus.listen('Overlay:show', () => {
      if (!hasOverlay()) {
        buildOverlay();
      }

      showOverlay();
    });

    Theme.EventBus.listen('Overlay:hide', () => hideOverlay());
  }

  /**
   * Set click events.
   */
  function setClickEvents() {
    const overlay = document.querySelector(selectors.container);

    on('click', overlay, () => {
      Theme.EventBus.emit('Overlay:on:click');
    });
  }

  /**
   * Show the overlay.
   */
  function showOverlay() {
    const overlay = document.querySelector(selectors.container);
    overlay.classList.add(cssClasses.active);
  }

  /**
   * Hide the overlay.
   */
  function hideOverlay() {
    const overlay = document.querySelector(selectors.container);
    overlay.classList.remove(cssClasses.active);
  }

  /**
   * Build overlay.
   */
  function buildOverlay() {
    const element = document.createElement('div');

    element.classList.add('overlay');
    document.body.appendChild(element);
  }

  /**
   * Returns if the overlay exists.
   */
  function hasOverlay() {
    return document.querySelector(selectors.container);
  }

  /**
   * Expose public interface.
   */
  return Object.freeze({
    init,
  });
}
