/**
 * Section: Feature Toggle
 * ------------------------------------------------------------------------------
 * Toggleable tabs.
 *
 * @namespace featureToggle
 */
import {register} from '../helpers/sections';
import Tabs from '../components/tabs';

/**
 * Register a new `feature-toggle` section.
 */
register('feature-toggle', {

  /**
   * Initiate the section.
   */
  init() {
    this.registerTabs();
  },

  /**
   * Register tabs.
   */
  registerTabs() {
    Tabs(this.selector, {
      autoPlay: true,
    }).init();
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});