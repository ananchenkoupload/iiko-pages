/**
 * Section: Feature Tabs
 * ------------------------------------------------------------------------------
 * Toggleable tabs.
 *
 * @namespace featureTabs
 */
import {register} from '../helpers/sections';
import Tabs from '../components/tabs';

/**
 * Register a new `feature-tabs` section.
 */
register('feature-tabs', {

  /**
   * Initiate the section.
   */
  init() {
    this.registerTabs();
  },

  /**
   * Register tabs.
   */
  registerTabs() {
    Tabs(this.selector, {
      autoPlay: true,
    }).init();
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});