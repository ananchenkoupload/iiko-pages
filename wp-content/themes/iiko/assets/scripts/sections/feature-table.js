/**
 * Section: Feature Table
 * ------------------------------------------------------------------------------
 * Toggles sub rows.
 *
 * @namespace featureTable
 */
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';

/**
 * Selectors.
 */
const selectors = {
  row: '[js-feature-table="row"]',
  rowToggle: '[js-feature-table="rowToggle"]',
};

/**
 * Register a new `feature-table` section.
 */
register('feature-table', {

  /**
   * Register node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      row: this.container.querySelectorAll(selectors.row),
    };
  },

  /**
   * Initiate the section.
   */
  init() {
    this.setNodeSelectors();
    this.setListeners();
  },

  /**
   * Set listeners.
   */
  setListeners() {
    [...this.nodeSelectors.row].forEach((element) => {
      on('click', element, (event) => this.handleRowClickEvent(event));
    });
  },

  /**
   * Handle the row toggle click event.
   * @param {object} event - The event object.
   */
  handleRowClickEvent(event) {
    const target = event.currentTarget;
    const row = target.dataset.row;

    this.toggleRow(row);
  },

  /**
   * Toggles a row.
   * @param {string} id - The row id.
   */
  toggleRow(id) {
    return this.rowIsOpen(id) ? this.closeRow(id) : this.openRow(id);
  },

  /**
   * Returns if a row is open.
   * @param {string} id - The row id.
   */
  rowIsOpen(id) {
    return this.getRow(id)[0].classList.contains(cssClasses.active);
  },

  /**
   * Returns a row.
   * @param {string} id - The row id.
   */
  getRow(id) {
    return this.container.querySelectorAll(`${selectors.row}[data-row="${id}"]`);
  },

  /**
   * Returns a row toggle.
   * @param {string} id - The row id.
   */
  getRowToggle(id) {
    return this.container.querySelector(`${selectors.row}[data-row="${id}"]`).querySelector(selectors.rowToggle);
  },

  /**
   * Opens a row.
   * @param {string} id - The row id.
   */
  openRow(id) {
    [...this.getRow(id)].forEach((element) => {
      element.classList.add(cssClasses.active);
    });
  
    this.getRowToggle(id).classList.add(cssClasses.active);
  },

  /**
   * Closes a row.
   * @param {string} id - The row id.
   */
  closeRow(id) {
    [...this.getRow(id)].forEach((element) => {
      element.classList.remove(cssClasses.active);
    });

    this.getRowToggle(id).classList.remove(cssClasses.active);
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});