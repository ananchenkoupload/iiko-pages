/**
 * Section: Testimonials
 * ------------------------------------------------------------------------------
 * Displays a video and testimonial carousel.
 *
 * @namespace testimonials
 */
import Flickity from 'flickity';
import YouTubePlayer from 'youtube-player';
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';

/**
 * Selectors.
 */
const selectors = {
  carousel: '[js-testimonials="carousel"]',
  video: '[js-testimonials="video"]',
  videoPlay: '[js-testimonials="videoPlay"]',
};

/**
 * Register a new `testimonials` section.
 */
register('testimonials', {

  /**
   * Initiate the component.
   */
  init() {
    this.setCarouselConfig();
    this.registerCarousel();
    this.setNodeSelectors();
    this.setResizeEvents();
    
    if (this.nodeSelectors.video) {
      this.setVideoDefaults();
    }
  },

  /**
   * Set the resize events.
   */
  setResizeEvents() {
    on('resize', () => {
      if (window.matchMedia('(max-width: 1024px)').matches) {
        this.setCarouselConfig();
        this.carousel.destroy();
        this.registerCarousel();
        return;
      }
    });
  },

  /**
   * Set the carousel config.
   */
  setCarouselConfig() {
    const bodyClassList = document.body.classList;
    const mediaQuery = '(max-width: 1024px)';
    const matchMedia = window.matchMedia(mediaQuery).matches;

    if (bodyClassList.contains('page-template-support') && !matchMedia) {
      this.carouselConfig = {
        autoPlay: 4000,
        cellAlign: 'right',
        lazyLoad: 1,
        rightToLeft: true,
        pageDots: true,
        pauseAutoPlayOnHover: false,
        prevNextButtons: false,
      };

      return;
    }

    this.carouselConfig = {
      autoPlay: 4000,
      cellAlign: 'left',
      lazyLoad: 1,
      pageDots: true,
      pauseAutoPlayOnHover: false,
      prevNextButtons: false,
    };
  },

  /**
   * Set node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      video: this.container.querySelector(selectors.video),
      videoPlay: this.container.querySelector(selectors.videoPlay),
    };
  },

  /**
   * Set video defaults.
   */
  setVideoDefaults() {
    this.registerYouTubePlayer();
    this.setVideoListeners();
  },

  /**
   * Set listeners.
   */
  setVideoListeners() {
    on('click', this.nodeSelectors.videoPlay, () => this.handlePlayClick());
  },

  /**
   * Handles the play button click event.
   */
  handlePlayClick() {
    const videoEmbed = this.container.querySelector(selectors.video);
      
    this.player.getPlayerState().then((response) => {
      if (response === 1) {
        this.player.pauseVideo();
        videoEmbed.classList.remove(cssClasses.active);
        this.nodeSelectors.videoPlay.classList.remove(cssClasses.hidden);

        return;
      }

      this.player.playVideo();
      videoEmbed.classList.add(cssClasses.active);
      this.nodeSelectors.videoPlay.classList.add(cssClasses.hidden);
    });
  },

  /**
   * Register the YouTube player.
   */
  registerYouTubePlayer() {
    this.player = YouTubePlayer(this.nodeSelectors.video, {
      height: '100%',
      width: '100%',
      playerVars: {
        showinfo: 0,
        rel: 0,
        modestbranding: 1,
      },
    });

    this.player.loadVideoById(this.nodeSelectors.video.dataset.video);
    this.player.stopVideo();
  },

  /**
   * Register the carousel.
   */
  registerCarousel() {
    this.carousel = new Flickity(selectors.carousel, this.carouselConfig);
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  }
});