/**
 * Section: Feature Grid
 * ------------------------------------------------------------------------------
 * Displays content with feature cards in a carousel.
 *
 * @namespace featureGrid
 */
import Flickity from 'flickity';
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';

/**
 * Selectors.
 */
const selectors = {
  cell: '[js-feature-grid="cell"]',
  items: '[js-feature-grid="items"]',
};

/**
 * Register a new `feature-grid` section.
 */
register('feature-grid', {

  /**
   * Initiate the section.
   */
  init() {
    this.registerCarousel();
  },

  /**
   * Register the carousel.
   */
  registerCarousel() {
    this.carousel = new Flickity(selectors.items, {
      cellAlign: 'left',
      cellSelector: selectors.cell,
      freeScroll: true,
    });

    this.carouselRegistered = true;
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});