/**
 * Section: Latest News
 * ------------------------------------------------------------------------------
 * Displays content with feature cards in a carousel.
 *
 * @namespace latestNews
 */
import Flickity from 'flickity';
import {register} from '../helpers/sections';

/**
 * Selectors.
 */
const selectors = {
  carousel: '[js-latest-news="carousel"]',
  cell: '[js-latest-news="cell"]',
};

/**
 * Register a new `latest-news` section.
 */
register('latest-news', {

  /**
   * Initiate the component.
   */
  init() {
    this.setNodeSelectors();
    this.registerCarousel();
  },

  /**
   * Set node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      cell: this.container.querySelectorAll(selectors.cell),
    };
  },

  /**
   * Register the carousel.
   */
  registerCarousel() {
    this.carousel = new Flickity(selectors.carousel, {
      autoPlay: 4000,
      cellAlign: 'left',
      pageDots: true,
      pauseAutoPlayOnHover: false,
      prevNextButtons: false,
    });

    window.setTimeout(() => {
      [...this.nodeSelectors.cell].forEach((element) => {
        element.classList.add('is-resized');
      });
    }, 250);
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  }
});