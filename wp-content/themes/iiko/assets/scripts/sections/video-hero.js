/**
 * Section: Video Hero
 * ------------------------------------------------------------------------------
 * A video hero banner.
 *
 * @namespace videoHero
 */
import {register} from '../helpers/sections';
import {on} from '../helpers/utils';
import cssClasses from '../helpers/css-classes';

/**
 * Selectors.
 */
const selectors = {
  mute: '[js-video-hero="mute"]',
  video: '[js-video-hero="video"]',
};

/**
 * Global instances.
 */
let playerMuted = false;

/**
 * Register a new `video-hero` section.
 */
register('video-hero', {

  /**
   * Register node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      mute: this.container.querySelector(selectors.mute),
      video: this.container.querySelector(selectors.video),
    };
  },

  /**
   * Initiate the section.
   */
  init() {
    this.setNodeSelectors();
    this.setListeners();
  },

  /**
   * Set listeners.
   */
  setListeners() {
    on('click', this.nodeSelectors.mute, () => this.handleMuteClick());
  },

  /**
   * Handle the mute event.
   */
  handleMuteClick() {
    return playerMuted ? this.unMutePlayer() : this.mutePlayer();
  },

  /**
   * Mutes the YouTube player.
   */
  mutePlayer() {
    this.nodeSelectors.video.muted = true;
    this.nodeSelectors.mute.classList.remove(cssClasses.active);

    playerMuted = true;
  },

  /**
   * Unmute the YouTube player.
   */
  unMutePlayer() {
    this.nodeSelectors.video.muted = false;
    this.nodeSelectors.mute.classList.add(cssClasses.active);

    playerMuted = false;
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  },
});