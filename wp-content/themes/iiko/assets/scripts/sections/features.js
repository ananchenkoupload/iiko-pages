/**
 * Section: Features
 * ------------------------------------------------------------------------------
 * Displays content with feature cards in a carousel.
 *
 * @namespace features
 */
import Flickity from 'flickity';
import {register} from '../helpers/sections';

/**
 * Selectors.
 */
const selectors = {
  carousel: '[js-features="carousel"]',
  cell: '[js-features="cell"]',
};

/**
 * Register a new `features` section.
 */
register('features', {

  /**
   * Initiate the component.
   */
  init() {
    this.registerCarousel();
    this.setNodeSelectors();
  },

  /**
   * Set node selectors.
   */
  setNodeSelectors() {
    this.nodeSelectors = {
      cell: document.querySelectorAll(selectors.cell),
    };
  },

  /**
   * Register the carousel.
   */
  registerCarousel() {
    this.carousel = new Flickity(selectors.carousel, {
      autoPlay: 4000,
      cellAlign: 'left',
      pageDots: true,
      pauseAutoPlayOnHover: false,
      prevNextButtons: false,
    });

    window.setTimeout(() => {
      [...this.nodeSelectors.cell].forEach((element) => {
        element.classList.add('is-resized');
      });
    }, 250);
  },

  /**
   * On load.
   */
  onLoad() {
    this.init();
  }
});