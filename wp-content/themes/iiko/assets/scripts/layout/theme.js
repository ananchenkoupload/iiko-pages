/**
 * Layout: Theme
 * ------------------------------------------------------------------------------
 * Entry file for all global theme scripts.
 * - Runs on every page template.
 */
import 'babel-polyfill';
import 'lazysizes';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes/plugins/respimg/ls.respimg.min.js';

/**
 * Global theme helpers.
 */
import SmoothScroll from 'smooth-scroll';
import EventBus from '../helpers/event-bus';
import Overlay from '../helpers/overlay';
import {load} from '../helpers/sections';
import '../helpers/polyfills';
import Toggle from '../helpers/toggle';

/**
 * Import theme components.
 */
import MenuDrawer from '../components/menu-drawer';
import SiteHeader from '../components/site-header';

/**
 * Import theme sections.
 */
import '../sections/features';
import '../sections/feature-grid';
import '../sections/feature-table';
import '../sections/feature-tabs';
import '../sections/feature-toggle';
import '../sections/highlighted';
import '../sections/latest-news';
import '../sections/testimonials';
import '../sections/video';
import '../sections/video-carousel';
import '../sections/video-hero';

/**
 * Global theme utilities.
 */
window.Theme = window.Theme || {};
window.Theme.EventBus = EventBus();

/**
 * Initialise smooth scroll.
 */
window.Theme.SmoothScroll = new SmoothScroll('a[href*="#"]', {
  speed: 500,
  updateURL: false,
});

document.addEventListener('DOMContentLoaded', () => {

  /**
   * Initiate helpers.
   */
  Overlay().init();
  load('*');

  /**
   * Initiate components.
   */
  MenuDrawer().init();
  SiteHeader().init();

  const modalOpenToggles = [...document.querySelectorAll('[js-modal="open"]')]
  modalOpenToggles.forEach((button) => {
    button.addEventListener('click', function(e) {
      e.preventDefault()
      const target = this.getAttribute('href')
      const targetEl = document.querySelector(target)

      targetEl.classList.add('is-active')
    })
  })

  const modalCloseToggles = [...document.querySelectorAll('[js-modal="close"]')]
  modalCloseToggles.forEach((button) => {
    button.addEventListener('click', function (e) {
      e.preventDefault()
      const targetEl = document.querySelector('.ll-downpopup.is-active')

      targetEl.classList.remove('is-active')
    })
  })


  /**
   * Menu drawer.
   */
  Toggle({
    namespace: 'MenuDrawer',
    overlay: true,
  }).init();
});
